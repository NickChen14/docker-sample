-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018 年 07 月 05 日 16:02
-- 伺服器版本: 5.7.22
-- PHP 版本： 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `sample`
--
CREATE DATABASE IF NOT EXISTS `sample` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sample`;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_sample`
--

DROP TABLE IF EXISTS `tbl_sample`;
CREATE TABLE `tbl_sample` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '流水號',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `tel` varchar(20) NOT NULL COMMENT '電話',
  `avatar` varchar(5) NOT NULL COMMENT '圖片副檔名',
  `order_num` int(10) UNSIGNED NOT NULL COMMENT '排序',
  `update_datetime` datetime NOT NULL COMMENT '修改時間',
  `create_datetime` datetime NOT NULL COMMENT '建立時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `tbl_sample`
--

INSERT INTO `tbl_sample` (`id`, `name`, `tel`, `avatar`, `order_num`, `update_datetime`, `create_datetime`) VALUES
(1, 'Nick', '0911000000', 'jpg', 4, '2018-07-03 08:24:26', '2018-07-03 08:24:26'),
(2, 'Shimin', '0922011010', 'jpeg', 5, '2018-07-03 11:16:12', '2018-07-03 11:16:12'),
(3, 'Vincent', '0911000001', 'jpg', 3, '2018-07-03 11:23:13', '2018-07-03 11:23:13'),
(4, 'Jasmine', '02-29919999', 'jpg', 2, '2018-07-03 11:24:09', '2018-07-03 11:24:09'),
(5, 'Kai', '0980888888', 'jpg', 1, '2018-07-03 11:40:39', '2018-07-03 11:27:16');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `tbl_sample`
--
ALTER TABLE `tbl_sample`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `tbl_sample`
--
ALTER TABLE `tbl_sample`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
