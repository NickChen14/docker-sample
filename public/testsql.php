<?php

try {
    $db = new PDO(
	    'mysql:host=sample-db;dbname=sample;charset=utf8',
		'root',
		'1234'
	);
} catch (PDOException $e) {
    echo 'DB connection failed';
	exit;
}

$data = $db->query('select * from tbl_sample');
$data = $data->fetchAll();
var_dump($data);

