FROM php:7.0-apache

COPY scripts/apache2/web.conf /etc/apache2/sites-available

RUN set -xe \
    && apt-get update && apt-get install -y \
        libxml2-dev \
        libmcrypt-dev \
        iputils-ping \
# PHP ext
    && docker-php-source extract \
    && docker-php-ext-install \
        mcrypt \
        mbstring \
        soap \
        mysqli \
        pdo \
        pdo_mysql \
    # && pecl install xdebug \
    # && docker-php-ext-enable xdebug \
    && docker-php-source delete \
# Apache
    && a2ensite web.conf \
    && a2dissite 000-default.conf \
    && a2enmod rewrite \
# Install composer
    && curl -Ss https://getcomposer.org/installer | php \
    && mv composer.phar /usr/bin/composer
